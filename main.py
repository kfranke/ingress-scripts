#!/usr/bin/env python

import cgi
import webapp2
import re

class MainHandler(webapp2.RequestHandler):
    def get(self):
		self.response.out.write("""
			<html>
				<body>
					<form action="/kml" method="post">
						<div>
					<textarea name="content" rows="3" cols="60"></textarea>
				</div>
					<div>
				<input type="submit" value="Convert Drawtools to KML">
				</div>
					</form>
				</body>
			</html>""")

class KmlOutput(webapp2.RequestHandler):
	def post(self):
		myin = self.request.get('content')
		style = ''
		
		## Write Header
		myout = "<?xml version='1.0' encoding='UTF-8'?>\n"
		myout += "<kml xmlns='http://www.opengis.net/kml/2.2'>\n"
		myout += "<Document>\n"
		myout += "<name>Drawtools Export</name>\n"
		myout += "<description><![CDATA[]]></description>\n"
		myout += "<Folder>\n"
		myout += "<name>Drawtools</name>\n"
		
		## Find our objects to add to the KML
		lines = re.finditer( r'{"type":"polyline".+?,"color":[^}]+?}', myin)
		polygon = re.finditer( r'{"type":"polygon".+?,"color":[^}]+?}', myin)
		markers = re.finditer( r'{"type":"marker".+?,"color":[^}]+?}', myin)
		
		## Lines
		linecount = 0
		for line in lines:
			foo = line.group()
			# apparently in KML hex colors are AABBGGRR.. so we'll have to rearrange 
			color =  re.search( r'"color":"#([^"]+)', foo)
			hexcode = color.group(1)
			
			# style
			style += "<Style id='line-"
			style += str(linecount)
			style += "'>\n"
			style += "<LineStyle>\n"
			style += "<color>FF"
			style += hexcode[-2:]
			style += hexcode[2:4]
			style += hexcode[:2]
			style += "</color>\n"
			style += "<width>1</width>\n"
			style += "</LineStyle>\n"
			style += "</Style>\n"
			
			# main output
			myout += "<Placemark>\n"
			myout += "<styleUrl>"
			myout += "#line-"
			myout += str(linecount)
			myout += "</styleUrl>\n"
			myout += "<name>"
			myout += "Line "
			myout += str(linecount)
			myout += "</name>\n"
			myout += "<ExtendedData>\n"
			myout += "</ExtendedData>\n"
			myout += "<LineString>\n"
			myout += "<tessellate>1</tessellate>\n"
			myout += "<coordinates>"
			
			# coordinates
			for point in re.finditer( r'"lat":([^,]+),"lng":([^}]+)', foo):
				myout += point.group(2)
				myout += ","
				myout += point.group(1)
				myout += ",0.0 "
			
			# closing tags
			myout += "</coordinates>\n"
			myout += "</LineString>\n"
			myout += "</Placemark>\n"
			
			linecount += 1
		
		
		## Polygons
		polycount = 0
		for poly in polygon:
			foo = poly.group()
			color = re.search( r'"color":"#([^"]+)', foo)
			hexcode = color.group(1)
			
			# style
			style += "<Style id='poly-"
			style += str(polycount)
			style += "'>\n"
			style += "<LineStyle>\n"
			style += "<color>FF"
			style += hexcode[-2:]
			style += hexcode[2:4]
			style += hexcode[:2]
			style += "</color>\n"
			style += "<width>1</width>\n"
			style += "</LineStyle>\n"
			style += "<PolyStyle>\n"
			# make the fill color semitransparent with 55 instead of FF 
			style += "<color>55"
			style += hexcode[-2:]
			style += hexcode[2:4]
			style += hexcode[:2]
			style += "</color>\n"
			style += "<fill>1</fill>\n"
			style += "<outline>1</outline>\n"
			style += "</PolyStyle>\n"
			style += "</Style>\n"
			
			# main output
			myout += "<Placemark>\n"
			myout += "<styleUrl>"
			myout += "#poly-"
			myout += str(polycount)
			myout += "</styleUrl>\n"
			myout += "<name>"
			myout += "Field "
			myout += str(polycount)
			myout += "</name>\n"
			myout += "<ExtendedData>\n"
			myout += "</ExtendedData>\n"
			myout += "<Polygon>\n"
			myout += "<outerBoundaryIs>\n"
			myout += "<LinearRing>\n"
			myout += "<tessellate>1</tessellate>\n"
			myout += "<coordinates>"
			
			# coordinates
			coordcount = 0
			first = ''
			block = ''
			for point in re.finditer( r'"lat":([^,]+),"lng":([^}]+)', foo):
				coordcount += 1
				block = point.group(2)
				block += ","
				block += point.group(1)
				block += ",0.0 "
				myout += block
				if coordcount == 1:
					first += block
			myout += first	
			
			# closing tags
			myout += "</coordinates>\n"
			myout += "</LinearRing>\n"
			myout += "</outerBoundaryIs>\n"
			myout += "</Polygon>\n"
			myout += "</Placemark>\n"
			
			polycount += 1
		
		
		## Markers
		markcount = 0
		for mark in markers:
			foo = mark.group()
			color = re.search( r'"color":"#([^"]+)', foo)
			hexcode = color.group(1)
			points = re.search( r'"lat":([^,]+),"lng":([^}]+)', foo)
			
			# style
			style += "<Style id='marker-"
			style += str(markcount)
			style += "'>\n"
			style += "<IconStyle>\n"
			style += "<color>FF"
			style += hexcode[-2:]
			style += hexcode[2:4]
			style += hexcode[:2]
			style += "</color>\n"
			style += "<scale>1.1</scale>\n"
			style += "<Icon>\n"
			style += "<href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href>\n"
			style += "</Icon>\n"
			style += "</IconStyle>\n"
			style += "</Style>\n"
			
			# main output
			myout += "<Placemark>\n"
			myout += "<styleUrl>"
			myout += "#marker-"
			myout += str(markcount)
			myout += "</styleUrl>\n"
			myout += "<name>"
			myout += "Marker "
			myout += str(markcount)
			myout += "</name>\n"
			myout += "<ExtendedData>\n"
			myout += "</ExtendedData>\n"
			myout += "<Point>\n"
			
			# coordinates
			myout += "<coordinates>"
			myout += points.group(2)
			myout += ","
			myout += points.group(1)
			myout += ",0.0"
			
			# closing tags
			myout += "</coordinates>\n"
			myout += "</Point>\n"
			myout += "</Placemark>\n"
			
			markcount += 1
		
		myout += "</Folder>\n"
	
		## Add Style
		myout += style
		myout += "</Document>\n"
		myout += "</kml>\n"
		
		## Print HTML
		self.response.out.write('<html><body><pre>')
		self.response.out.write(cgi.escape(myout))
		self.response.out.write('</pre></body></html>')
	
	
app = webapp2.WSGIApplication([
    ('/drawtools2kml', MainHandler),
	('/kml', KmlOutput)
], debug=True)
